package tdc.edu.vn.quanlynhansu;

import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import tdc.edu.vn.quanlynhansu.adapters.MyListViewAdapter;
import tdc.edu.vn.quanlynhansu.adapters.MyRecyclerViewAdapter;
import tdc.edu.vn.quanlynhansu.components.MyDialog;
import tdc.edu.vn.quanlynhansu.data_models.Person;
import tdc.edu.vn.quanlynhansu.database_layer.PersonDatabaseAPIs;

public class QLNS2021Activity extends AppCompatActivity {
    public static String TRUNGCAP = "Trung cap";
    public static String CAODANG = "Cao dang";
    public static String DAIHOC = "Dai hoc";
    public static String KHONGBANGCAP = "Khong bang cap";

    private int selectedRow = -1;
    private int backColor;
    private CardView previousItem;

    private EditText edtName, edtHoppies;
    private CheckBox chkRead, chkTravel;
    private RadioGroup radioGroup;
    private RadioButton radUniversity, radCollege, radTraining;
    private Button btnAdd;

    private ArrayList<Person> persons = new ArrayList<Person>();
    //private ArrayAdapter<Person> adapter;
    private MyRecyclerViewAdapter adapter;
    private PersonDatabaseAPIs DAO;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quan_ly_nhan_su_layout);

        //Get views from layout
        edtName = (EditText) findViewById(R.id.edtName);
        radioGroup = (RadioGroup) findViewById(R.id.radGroup);
        chkRead = (CheckBox) findViewById(R.id.chkRead);
        chkTravel = (CheckBox) findViewById(R.id.chkTravel);
        edtHoppies = (EditText) findViewById(R.id.edtHobbies);
        radUniversity = findViewById(R.id.radUniversity);
        radCollege = findViewById(R.id.radCollege);
        radTraining = findViewById(R.id.radTraining);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.listPerson);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        Button btnCancel = (Button) findViewById(R.id.btnCancel);

        adapter = new MyRecyclerViewAdapter(this, R.layout.listview_item, persons);

        // config layout manager for recycler view
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
//        recyclerView.setLayoutManager(linearLayoutManager);


//        Grid layout manager
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        gridLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(gridLayoutManager);

        // set Event processing on the Recylerview
        adapter.setOnClickListner(new MyRecyclerViewAdapter.onItemClickLisner() {
            @Override
            public void onItemClickListner(int position, View view) {
                    if (selectedRow == -1) {
                        selectedRow = position;
                        disableButtonAdd();
                        setPersonToLayout(persons.get(position));
                        // Change the item background color
                        CardView bgrItem = view.findViewById(R.id.bgrListItem);
                        backColor = bgrItem.getSolidColor();
                        bgrItem.setBackgroundColor(getResources().getColor(R.color.btnColor, getTheme()));
                        previousItem = bgrItem;
                    } else {
                        if (selectedRow == position) {//Tap on the same item
                            clear();
                            selectedRow = -1;
                            enableButtonAdd();
                            CardView bgrItem = view.findViewById(R.id.bgrListItem);
                            bgrItem.setBackgroundColor(backColor);
                        } else {// Tap on other item
                            previousItem.setBackgroundColor(backColor);
                            selectedRow = position;
                            disableButtonAdd();
                            setPersonToLayout(persons.get((int) selectedRow));
                            CardView bgrItem = view.findViewById(R.id.bgrListItem);
                            backColor = bgrItem.getSolidColor();
                            bgrItem.setBackgroundColor(getResources().getColor(R.color.btnColor, getTheme()));
                            previousItem = bgrItem;
                        }
                    }
                }
        });

        // Set Adapter
        recyclerView.setAdapter(adapter);

      /*  recyclerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (selectedRow == -1) {
                    selectedRow = i;
                    disableButtonAdd();
                    setPersonToLayout(persons.get(i));
                    // Change the item background color
                    LinearLayout bgrItem = view.findViewById(R.id.bgrListItem);
                    backColor = bgrItem.getSolidColor();
                    bgrItem.setBackgroundColor(getResources().getColor(R.color.btnColor, getTheme()));
                    previousItem = bgrItem;
                } else {
                    if (selectedRow == i) {//Tap on the same item
                        clear();
                        selectedRow = -1;
                        enableButtonAdd();
                        LinearLayout bgrItem = view.findViewById(R.id.bgrListItem);
                        bgrItem.setBackgroundColor(backColor);
                    } else {// Tap on other item
                        previousItem.setBackgroundColor(backColor);

                        selectedRow = i;
                        disableButtonAdd();
                        setPersonToLayout(persons.get((int) selectedRow));
                        LinearLayout bgrItem = view.findViewById(R.id.bgrListItem);
                        backColor = bgrItem.getSolidColor();
                        bgrItem.setBackgroundColor(getResources().getColor(R.color.btnColor, getTheme()));
                        previousItem = bgrItem;
                    }
                }
            }
        });
*/
        // Load database
        DAO = new PersonDatabaseAPIs(this);
        DAO.getAllPerson(persons, adapter);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedRow == -1) {
                    Person nhanSu = createPersonFromLayout();
                    if (nhanSu != null) {
                        // Add to the person list
                        //DAO.savePerson(nhanSu);
                        persons.add(nhanSu);
                        // Bao cho adapter biet du lieu thay doi
                        adapter.notifyDataSetChanged();
                        clear();
                    }
                } else {

                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private Person createPersonFromLayout() {
        Person nhanSu = null;
        if (!edtName.getText().toString().isEmpty()) {
            nhanSu = new Person();
            nhanSu.setPersonName(edtName.getText().toString());
            int id = radioGroup.getCheckedRadioButtonId();
            switch (id) {
                case R.id.radTraining:
                    nhanSu.setPersonDegree(TRUNGCAP);
                    break;
                case R.id.radCollege:
                    nhanSu.setPersonDegree(CAODANG);
                    break;
                case R.id.radUniversity:
                    nhanSu.setPersonDegree(DAIHOC);
                    break;
                default:
                    nhanSu.setPersonDegree(KHONGBANGCAP);
                    break;
            }
            String hoppies = "";
            ArrayList<String> myHobbies = new ArrayList<String>();
            if (chkRead.isChecked()) {
                hoppies += chkRead.getText() + ";";
                myHobbies.add((String) chkRead.getText());
            }
            if (chkTravel.isChecked()) {
                hoppies += chkTravel.getText() + ";";
                myHobbies.add((String) chkTravel.getText());

            }
            hoppies += edtHoppies.getText();
            myHobbies.add(edtHoppies.getText().toString());
            String hb = "";
//            if (myHobbies.size() > 0) {
                hb = "Hobbies: " + String.join(", ", myHobbies);
//            }
            nhanSu.setPersonHoppies(hb);
        }
        return nhanSu;
    }

    private void setPersonToLayout(Person person) {
        //TODO setPerson
        clear();
        edtName.setText(person.getPersonName());
        if (person.getPersonDegree().equalsIgnoreCase(DAIHOC)) {
            radUniversity.setChecked(true);
        } else if (person.getPersonDegree().equalsIgnoreCase(CAODANG)) {
            radCollege.setChecked(true);
        } else if (person.getPersonDegree().equalsIgnoreCase(TRUNGCAP)) {
            radTraining.setChecked(true);
        }

        StringTokenizer tokenizer = new StringTokenizer(person.getPersonHoppies());
        String other = "";
        if (person.getPersonHoppies().contains(chkRead.getText().toString())) {
            chkRead.setChecked(true);
            tokenizer.nextToken(";");
        }
        if (person.getPersonHoppies().contains(chkTravel.getText().toString())) {
            chkTravel.setChecked(true);
            tokenizer.nextToken(";");
        }
        if (tokenizer.hasMoreTokens()) {
            other = tokenizer.nextToken(";");
            edtHoppies.setText(other);
        }

    }

    private void clear() {
        //TODO clear
        edtName.setText("");
        radioGroup.clearCheck();
        chkRead.setChecked(false);
        chkTravel.setChecked(false);
        edtHoppies.setText("");
        edtName.requestFocus();
    }

    private void disableButtonAdd() {
        btnAdd.setEnabled(false);
    }

    private void enableButtonAdd() {
        btnAdd.setEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return true;
    }
    // Processing of Option Menu

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnuSave:
                save();
                return true;
            //break;
            case R.id.mnuUpdate:
                update();
                return true;
            //break;
            case R.id.mnuDelete:
                delete();
                return true;
            //break;
            default:
                return super.onOptionsItemSelected(item);
            //return false;
        }
        //return super.onOptionsItemSelected(item);
    }

    private void save() {
        //TODO save
        //Toast.makeText(this, "Save()", Toast.LENGTH_LONG).show();
        int count = DAO.saveAllPersionVersion2(persons);
        MyDialog dialog = new MyDialog(this);
        if (count == 0) {
            dialog.setMessage("No one is saved into the database!");
        } else {
            dialog.setMessage("You just saved " + count + " person into the database!");
        }
        dialog.show();
    }

    private void update() {
        //TODO update
        if (selectedRow != -1) {
            Person person = createPersonFromLayout();
            if (person != null) {
                Toast.makeText(this, "Update()", Toast.LENGTH_LONG).show();
                persons.get(selectedRow).setPersonName(person.getPersonName());
                persons.get(selectedRow).setPersonDegree(person.getPersonDegree());
                persons.get(selectedRow).setPersonHoppies(person.getPersonHoppies());
                if (persons.get(selectedRow).getId() != 0) { // New
                    DAO.updatePerson(persons.get(selectedRow));
                }
                adapter.notifyDataSetChanged();
                clear();
                selectedRow = -1;
                enableButtonAdd();
            }
        }
    }

    private void delete() {
        //TODO del
        Toast.makeText(this, "Delete()", Toast.LENGTH_LONG).show();
        if (selectedRow != -1) {
            DAO.delPerson(persons.get((int) selectedRow));
            clear();
            persons.remove(selectedRow);
            adapter.notifyDataSetChanged();
            selectedRow = -1;
            enableButtonAdd();
        }
    }
}
