package tdc.edu.vn.quanlynhansu.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import tdc.edu.vn.quanlynhansu.QLNS2021Activity;
import tdc.edu.vn.quanlynhansu.R;
import tdc.edu.vn.quanlynhansu.data_models.Person;

public class MyListViewAdapter extends ArrayAdapter<Person> {
    private Activity context;
    private int layoutID;
    private ArrayList<Person> persons;

    // Constructor
    public MyListViewAdapter(@NonNull Activity context, int layoutID, @NonNull ArrayList<Person> persons) {
        super(context, layoutID, persons);
        this.context = context;
        this.layoutID = layoutID;
        this.persons = persons;
    }
    // Define the getView method

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = null;
        MyViewHolder viewHolder;
        if (convertView == null) {
            view = context.getLayoutInflater().inflate(layoutID, parent, false);
            viewHolder = new MyViewHolder();
            // Get view-items from view oject
            viewHolder.imgDegree = view.findViewById(R.id.imgDegree);
            viewHolder.lblHoten = view.findViewById(R.id.lblHoten);
            viewHolder.lblSoThich = view.findViewById(R.id.lblSoThich);
            // Get data from data source (persons)
            Person person = persons.get(position);
            viewHolder.lblHoten.setText(person.getPersonName());
            viewHolder.lblSoThich.setText(person.getPersonHoppies());
            if (person.getPersonDegree().equalsIgnoreCase(QLNS2021Activity.DAIHOC)) {
                viewHolder.imgDegree.setImageResource(R.mipmap.university);
            }
            else if (person.getPersonDegree().equalsIgnoreCase(QLNS2021Activity.CAODANG)) {
                viewHolder.imgDegree.setImageResource(R.mipmap.college);
            }
            else if (person.getPersonDegree().equalsIgnoreCase(QLNS2021Activity.TRUNGCAP)) {
                viewHolder.imgDegree.setImageResource(R.mipmap.midium);
            }
            else {
                viewHolder.imgDegree.setImageResource(R.mipmap.none);
            }

            // Binding the viewholder to the new listview item
            view.setTag(viewHolder);
        }
        else {
            view = convertView;
            viewHolder = (MyViewHolder) view.getTag();

            // Get data from data source (persons)
            Person person = persons.get(position);
            viewHolder.lblHoten.setText(person.getPersonName());
            viewHolder.lblSoThich.setText(person.getPersonHoppies());
            if (person.getPersonDegree().equalsIgnoreCase(QLNS2021Activity.DAIHOC)) {
                viewHolder.imgDegree.setImageResource(R.mipmap.university);
            }
            else if (person.getPersonDegree().equalsIgnoreCase(QLNS2021Activity.CAODANG)) {
                viewHolder.imgDegree.setImageResource(R.mipmap.college);
            }
            else if (person.getPersonDegree().equalsIgnoreCase(QLNS2021Activity.TRUNGCAP)) {
                viewHolder.imgDegree.setImageResource(R.mipmap.midium);
            }
            else {
                viewHolder.imgDegree.setImageResource(R.mipmap.none);
            }
        }

        return view;
    }


    private static class MyViewHolder {
        ImageView imgDegree;
        TextView lblHoten;
        TextView lblSoThich;
    }
}
