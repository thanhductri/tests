package tdc.edu.vn.quanlynhansu.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import tdc.edu.vn.quanlynhansu.QLNS2021Activity;
import tdc.edu.vn.quanlynhansu.R;
import tdc.edu.vn.quanlynhansu.data_models.Person;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.MyViewHolder> {
    private Activity context;
    private int layoutId;
    private ArrayList<Person> personList;
    private  onItemClickLisner OnClickListner;

    // constructor
    public MyRecyclerViewAdapter(Activity context, int layoutId, ArrayList<Person> personList) {
        this.context = context;
        this.layoutId = layoutId;
        this.personList = personList;
    }

    // implement method
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardView cardViewItem = (CardView) context.getLayoutInflater().inflate(viewType, parent, false);
        return new MyViewHolder(cardViewItem);
    }

    // implement method
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Person person = personList.get(position);
        if (person.getPersonDegree().equalsIgnoreCase(QLNS2021Activity.DAIHOC)) {
            holder.imgDegree.setImageResource(R.mipmap.university);
        } else if (person.getPersonDegree().equalsIgnoreCase(QLNS2021Activity.CAODANG)) {
            holder.imgDegree.setImageResource(R.mipmap.college);
        } else if (person.getPersonDegree().equalsIgnoreCase(QLNS2021Activity.TRUNGCAP)) {
            holder.imgDegree.setImageResource(R.mipmap.midium);
        } else {
            holder.imgDegree.setImageResource(R.mipmap.none);
        }
        holder.lblHoten.setText(person.getPersonName());
        holder.lblSoThich.setText(person.getPersonHoppies());

        //Event processing
        final int pos = position;
        holder.onClickListener=new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(OnClickListner!=null){
                    OnClickListner.onItemClickListner(pos,holder.itemView);
                }
            }
        };
    }

    // implement method
    @Override
    public int getItemCount() {
        return personList.size();
    }

    // type by hand
    @Override
    public int getItemViewType(int position) {
        return layoutId;
    }

    // View holder definition
    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgDegree;
        TextView lblHoten;
        TextView lblSoThich;
        View.OnClickListener onClickListener;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgDegree = itemView.findViewById(R.id.imgDegree);
            lblHoten = itemView.findViewById(R.id.lblHoten);
            lblSoThich = itemView.findViewById(R.id.lblSoThich);

//            set event processing
            imgDegree.setOnClickListener(this);
            lblHoten.setOnClickListener(this);
            lblSoThich.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(onClickListener != null){
                onClickListener.onClick(view);
            }
            //Log.d("Click", "call");
        }
    }

    //interface for event processing
    public interface onItemClickLisner{
        public void onItemClickListner(int position, View view);
    }

    public void setOnClickListner(onItemClickLisner onClickListner) {
        OnClickListner = onClickListner;
    }

}
