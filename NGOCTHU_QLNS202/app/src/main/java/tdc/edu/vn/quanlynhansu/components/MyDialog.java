package tdc.edu.vn.quanlynhansu.components;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import tdc.edu.vn.quanlynhansu.R;

public class MyDialog {
    private Activity context;
    private AlertDialog.Builder builder;
    private AlertDialog dialog;
    private TextView lblMess;

    public MyDialog(Activity context) {
        this.context = context;
        builder = new AlertDialog.Builder(context, R.style.MyDialog);
        ViewGroup viewGroup = context.findViewById(android.R.id.content);
        View dialogView = context.getLayoutInflater().inflate(R.layout.dialog_layout, viewGroup, false);
        lblMess = dialogView.findViewById(R.id.lblMess);
        Button btnOK = dialogView.findViewById(R.id.btnOk);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        builder.setView(dialogView);
        dialog = builder.create();
    }

    public void show() {
        dialog.show();
    }
    public void dismiss() {
        dialog.dismiss();
    }
    public void setMessage(String message) {
        lblMess.setText(message);
    }
}
