package tdc.edu.vn.quanlynhansu.data_models;



import static tdc.edu.vn.quanlynhansu.data_models.Person.TABLE_NAME;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = TABLE_NAME)
public class Person {
    @Ignore
    public static final String TABLE_NAME = "person_table";
    @Ignore
    public static final String NAME = "name";
    @Ignore
    public static final String DEGREE = "degree";
    @Ignore
    public static final String HOPPIES = "hoppies";

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private long id;

    @ColumnInfo(name = NAME)
    private String personName;
    @ColumnInfo(name = DEGREE)
    private String personDegree;
    @ColumnInfo(name = HOPPIES)
    private String personHoppies;

    // Setter and getter


    public long getId() {
        return id;
    }

    public String getPersonName() {
        return personName;
    }

    public String getPersonDegree() {
        return personDegree;
    }

    public String getPersonHoppies() {
        return personHoppies;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public void setPersonDegree(String personDegree) {
        this.personDegree = personDegree;
    }

    public void setPersonHoppies(String personHoppies) {
        this.personHoppies = personHoppies;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return personName + " : " + personDegree + " : " + personHoppies;
    }
}
