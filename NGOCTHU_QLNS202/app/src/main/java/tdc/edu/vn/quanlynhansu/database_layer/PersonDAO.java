package tdc.edu.vn.quanlynhansu.database_layer;




import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.ArrayList;
import java.util.List;

import tdc.edu.vn.quanlynhansu.data_models.Person;

@Dao
public interface PersonDAO {
    @Query("SELECT * FROM " + Person.TABLE_NAME + " ORDER BY "+ Person.NAME + " ASC")
    List<Person> getAllPerson();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void saveAllPerson(ArrayList<Person> people);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long savePerson(Person person);

    @Delete
    int delPerson (Person person);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int updatePersion(Person person);
}
