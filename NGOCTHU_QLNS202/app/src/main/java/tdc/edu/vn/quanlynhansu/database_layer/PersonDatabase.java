package tdc.edu.vn.quanlynhansu.database_layer;

import static tdc.edu.vn.quanlynhansu.database_layer.PersonDatabase.DB_VERSION;


import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;


import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import tdc.edu.vn.quanlynhansu.data_models.Person;

@Database(entities = {Person.class}, version = DB_VERSION)
public abstract class PersonDatabase extends RoomDatabase {
    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "person_database";

    private static volatile PersonDatabase DATABASE;

    // Async task processing
    private static final int NUMBER_OF_EXEC_THREAD = 10;
    public static final ExecutorService dbReadWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_EXEC_THREAD);

    // Get person dao definition
    public abstract PersonDAO getPersonDao();

    // Get database definition
    public static PersonDatabase getDatabase(Context context){
        if (DATABASE == null) {
            synchronized (PersonDatabase.class) {
                DATABASE = Room.databaseBuilder(context.getApplicationContext(), PersonDatabase.class, DB_NAME).build();
            }
        }
        return DATABASE;
    }
}
