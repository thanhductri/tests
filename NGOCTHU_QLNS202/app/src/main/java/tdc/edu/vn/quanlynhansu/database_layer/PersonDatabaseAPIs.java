package tdc.edu.vn.quanlynhansu.database_layer;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import tdc.edu.vn.quanlynhansu.adapters.MyListViewAdapter;
import tdc.edu.vn.quanlynhansu.adapters.MyRecyclerViewAdapter;
import tdc.edu.vn.quanlynhansu.data_models.Person;

public class PersonDatabaseAPIs {
    private PersonDatabase database;
    private PersonDAO DAO;

    public PersonDatabaseAPIs(Context context) {
        database = PersonDatabase.getDatabase(context);
        DAO = database.getPersonDao();
    }

    private class Temp {
        private long position;
        private boolean isComplete;
    }

    // Database APIs Definition
    public void saveAllPerson(final ArrayList<Person> people) {
        PersonDatabase.dbReadWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                DAO.saveAllPerson(people);
            }
        });
    }
    public int saveAllPersionVersion2(ArrayList<Person> people){
        int saveCount = 0;
        for (Person person: people) {
            long position = savePerson(person);
            if (position != -1) {
                saveCount++;
            }
        }
        return saveCount;
    }
    public long savePerson(final Person person){
        final Temp temp = new Temp();
        PersonDatabase.dbReadWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                temp.isComplete = false;
                temp.position = DAO.savePerson(person);
                temp.isComplete = true;
            }
        });
        // Wait until the thread is completed
        while (!temp.isComplete) {
        }

        // Update the id in the table to person in the listview
        if (temp.position != -1) {
            person.setId(temp.position);
        }

        return temp.position;
    }
    public void getAllPerson (final ArrayList<Person> people, final MyRecyclerViewAdapter adapter) {
        // Load data from database
        PersonDatabase.dbReadWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                List<Person> list = DAO.getAllPerson();
                if (list.size() > 0) {
                    people.addAll(list);
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }/*    public void getAllPerson (final ArrayList<Person> people, final MyListViewAdapter adapter) {
        // Load data from database
        PersonDatabase.dbReadWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                List<Person> list = DAO.getAllPerson();
                if (list.size() > 0) {
                    people.addAll(list);
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }*/
    public int delPerson(final Person person){
        final Temp temp = new Temp();
        PersonDatabase.dbReadWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                temp.isComplete = false;
                temp.position = DAO.delPerson(person);
                temp.isComplete = true;
            }
        });
        while (!temp.isComplete){
        }
        return (int) temp.position;
    }
    public int updatePerson(final Person person) {
        final Temp temp = new Temp();
        PersonDatabase.dbReadWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                temp.isComplete = false;
                temp.position = DAO.updatePersion(person);
                temp.isComplete = true;
            }
        });
        while (!temp.isComplete){
        }
        return (int) temp.position;
    }
}
